﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class RankingManager : MonoBehaviour {

    [SerializeField] private ScrollRect _scrollview;
    [SerializeField] private GameObject _content;
    [SerializeField] private GameObject _node;
    [SerializeField] int     _nodenumber;
    [SerializeField] float   _contentmaxpos;
    [SerializeField] private bool    _onscrolleffect;
    private float   _nodewidht;
    private float   _nodeheight;
    private float   _contentwidth;
    private float   _contentheight;
    private float   _limit;
    private List<RectTransform> _nodelist;
    private bool    _initflag;
    private 

    // Use this for initialization
    void Start () {
        _nodelist = new List<RectTransform>(); 
        _onscrolleffect = true;
        _initflag = false;
        _scrollview.verticalNormalizedPosition = 0.0f;
        _contentwidth  = _content.GetComponent<RectTransform>().sizeDelta.x;
        _contentheight = _content.GetComponent<RectTransform>().sizeDelta.y;
        _nodewidht     = _node.GetComponent<RectTransform>().sizeDelta.x;
        _nodeheight    = _node.GetComponent<RectTransform>().sizeDelta.y;
        _contentmaxpos = _content.GetComponent<RectTransform>().localPosition.y;
        _limit = -1;

        StreamReader sr = new StreamReader(Application.dataPath +"/RankingData.csv", Encoding.GetEncoding("Shift_JIS"));
        string Line;
        string[] data = new string[3];
        for (int cnt = 0; cnt < _content.transform.childCount; cnt++)
        {
            if ((Line = sr.ReadLine()) != null)
                data = Line.Split(',');

            if (_nodeheight * cnt >= 1080 / 2.0f && _limit == -1)
                _limit = cnt;
            GameObject tmpobj = _content.transform.GetChild(cnt).gameObject;
            Text[] Detail = tmpobj.transform.GetComponentsInChildren<Text>();
            for (int num = 0; num < Detail.Length; num++)
            {
                switch (Detail[num].name)
                {
                    case "Number":
                        Detail[num].text = data[0];
                        break;
                    case "Name":
                        Detail[num].text = data[1];
                        break;
                    case "Score":
                        Detail[num].text = data[2];
                        break;

                }
            }
            _nodelist.Add(tmpobj.GetComponent<RectTransform>());

        }

        Invoke("OffLayout", 1);


    }
	
	// Update is called once per frame
	void Update () {
        RankingScroll(50);
    }


    void RankingScroll(int rank)
    {
        int rankpara = rank;
        if (_onscrolleffect)
        {
            if (!_initflag)
            {
                InsertRanking(rank);
                _initflag = true;
            }

            float noderate    = _nodeheight / _contentmaxpos;
            float centererror = noderate / 2;
            if (rank < _limit)
            {
                rankpara = 0;
            }

            if (rank > 100 - _limit)
            {
                rankpara = 100;
            }

            _scrollview.verticalNormalizedPosition += 0.005f;

            if (rankpara <= _limit)
            {
                if (_scrollview.verticalNormalizedPosition >= 1.0f)
                {
                    _scrollview.verticalNormalizedPosition = 1.0f;
                    _onscrolleffect = false;
                    _nodelist[rank].transform.Find("MyRank").parent = transform.parent;
                    StartCoroutine("SeparateRanking", rank);
                }
            }
            else if (rankpara >= (_nodenumber - _limit))
            {
                if (_scrollview.verticalNormalizedPosition >= 0.0f)
                {
                    _scrollview.verticalNormalizedPosition = 0.0f;
                    _onscrolleffect = false;
                    _nodelist[rank].transform.Find("MyRank").parent = transform.parent;
                    StartCoroutine("SeparateRanking", rank);
                }

            }
            else
            {
                if (_scrollview.verticalNormalizedPosition >= (noderate * ((100 - rankpara) - _limit)) + centererror)
                {
                    _scrollview.verticalNormalizedPosition = (noderate * ((100 - rankpara) - _limit)) + centererror;
                    _onscrolleffect = false;
                    _nodelist[rank].transform.Find("MyRank").parent = transform.parent;
                    StartCoroutine("SeparateRanking", rank);
                }

            }

        }

    }

    void InsertRanking(int rank)
    {
        GameObject tmpobj = _content.transform.Find("MyRank").gameObject;// Instantiate(_node, _nodelist[rank - 1].transform);
        tmpobj.transform.parent = _nodelist[rank - 1].transform;
        tmpobj.GetComponent<RectTransform>().position = new Vector3( _nodelist[rank - 1].transform.position.x + _nodewidht,
                                                                     _nodelist[rank - 1].transform.position.y,
                                                                     _nodelist[rank - 1].transform.position.z);
        Text[] Detail = tmpobj.transform.GetComponentsInChildren<Text>();
        tmpobj.name = "MyRank";
        for (int num = 0; num < Detail.Length; num++)
        {
            switch (Detail[num].name)
            {
                case "Number":
                    Detail[num].text = rank.ToString();
                    break;
                case "Name":
                    Detail[num].text = "SUGIURA";
                    break;
                case "Score":
                    Detail[num].text = "999999";
                    break;
            }
        }
        _nodelist.Insert(rank - 1, tmpobj.GetComponent<RectTransform>());

    }

    private IEnumerator SeparateRanking(int rank)
    {
        float topy = _nodelist[rank].position.y;

        while ((topy - _nodeheight) < _nodelist[rank].position.y)
        {
            for (int cnt = rank; cnt <= _nodenumber; cnt++)
            {
                _nodelist[cnt].position = new Vector3(_nodelist[cnt].position.x, _nodelist[cnt].position.y - 0.5f, _nodelist[cnt].position.z);

            }
            yield return null;
        }

        while (_nodelist[rank].position.x <= _nodelist[rank - 1].position.x)
        {
            _nodelist[rank - 1].position = new Vector3(_nodelist[rank - 1].position.x + -1.0f, _nodelist[rank - 1].position.y, _nodelist[rank - 1].position.z);
            yield return null; 
        }

        _nodelist[rank - 1].position = new Vector3(_nodelist[rank].position.x, _nodelist[rank - 1].position.y, _nodelist[rank - 1].position.z);
        _nodelist[rank - 1].transform.parent = _content.transform;



        yield break;  
    }

    void OffLayout()
    {

        _content.GetComponent<VerticalLayoutGroup>().enabled = false;

    }

}
