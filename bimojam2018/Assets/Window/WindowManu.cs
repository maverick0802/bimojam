﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowManu : MonoBehaviour {

    [SerializeField] private GameObject[] _gameobject;
    [SerializeField] private Animator     _animator;
    private  ChengeUI[] _menuobject;
    [SerializeField]bool ismenu;
    int _menunum;

	// Use this for initialization
	void Start () {
        ismenu = false;
        _menuobject = new ChengeUI[_gameobject.Length];
        for (int cnt = 0; cnt < _gameobject.Length; cnt++)
            _menuobject[cnt] = _gameobject[cnt].GetComponent<ChengeUI>();
    }
	
	// Update is called once per frame
	void Update () {

        if (ismenu) {

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                _menuobject[_menunum].swap();
                if (_menunum < _menuobject.Length - 1)
                    _menunum++;
                else
                    _menunum = 0;
                _menuobject[_menunum].swap();

            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                _menuobject[_menunum].swap();
                if (_menunum > 0)
                    _menunum--;
                else
                    _menunum = _menuobject.Length  - 1;

                _menuobject[_menunum].swap();

            }

            if (Input.GetKeyDown(KeyCode.Space))
            {

                switch (_menunum)
                {

                    case 0:

                        break;
                    case 1:

                        break;

                    case 2:

                        break;

                }
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                _animator.SetTrigger("Close");
            }

        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            _animator.SetTrigger("Open");
        }


    }


    public void OnMenu()
    {
        ismenu = true;
    }

    public void OffMenu()
    {
        ismenu = false;
    }
}
