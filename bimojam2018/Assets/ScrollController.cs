﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollController : MonoBehaviour {

    ScrollRect _scrollview;
    public bool _onscroll { get; set; }
	// Use this for initialization
	void OnEnable () {
        _onscroll = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (_onscroll)
        {
            if (Input.GetAxis("Vertical") > 0.1f || Input.GetKey(KeyCode.UpArrow)) 
            {
                if (_scrollview.verticalNormalizedPosition <= 1)
                    _scrollview.verticalNormalizedPosition += 0.05f;
                else
                    _scrollview.verticalNormalizedPosition = 1.0f;

            }

            if (Input.GetAxis("Vertical") < -0.1f || Input.GetKey(KeyCode.DownArrow))
            {
                if (_scrollview.verticalNormalizedPosition >= 0)
                    _scrollview.verticalNormalizedPosition += 0.05f;
                else
                    _scrollview.verticalNormalizedPosition = 0.0f;
            }
        }
    }
}
